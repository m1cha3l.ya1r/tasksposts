// create connection to mongodb
import mongoose from 'mongoose';

// create schema class
let appSchema = mongoose.Schema;

// schema
let userSchema = new appSchema(
    {
        Name : String,
        Email : String,
        Street : String,
        City : String,
        Zipcode : Number,
        Tasks :
            [{
                title: String,
                completed: Boolean
            }],
        Posts :
            [{
                title: String,
                body: String
            }]
    }
)


export default mongoose.model('user', userSchema)