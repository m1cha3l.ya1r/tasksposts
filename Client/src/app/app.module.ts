import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { UserComponent } from './user/user.component';

import { MatGridListModule } from '@angular/material/grid-list'; 
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TaskComponent } from './task/task.component';
import { PostComponent } from './post/post.component';
import { NewUserComponent } from './new-user/new-user.component';
import { PostsTasksColsComponent } from './posts-tasks-cols/posts-tasks-cols.component'; 



const appRoutes: Routes = [
  {path: "", component: MainpageComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    MainpageComponent,
    UserComponent,
    TaskComponent,
    PostComponent,
    NewUserComponent,
    PostsTasksColsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
