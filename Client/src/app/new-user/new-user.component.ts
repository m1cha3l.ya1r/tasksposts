import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  constructor(private utils: UserService, private router: Router) { }

  subPost: Subscription;

  newuser: User = {
    Name: null,
    Email: null,
    Street: null,
    City: null,
    Zipcode: null,
    Posts: [],
    Tasks: []
  }

  AddUser()
  {
    if(this.newuser.Name!=null && this.newuser.Email!=null)
    {this.subPost = this.utils.postUser(this.newuser).subscribe(data => 
      {
        console.log(data)
        window.location.reload()
      })}
  }

  ngOnInit(): void {
  }

  ngOnDestroy()
  {
    if(this.subPost!=null)
    {this.subPost.unsubscribe()}
  }

}
