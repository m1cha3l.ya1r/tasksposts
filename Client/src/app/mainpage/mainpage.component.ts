import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {

  usersList: any;
  search: string;
  
  subAll: Subscription;
  

  filteredUserList: any;
  TasksPosts: boolean = false;
  focusUser: any
  newUserToggle: boolean = false

  constructor(private utils: UserService, private router: Router) { }

  ngOnInit(): void {
    this.getAll()
  }

  getAll()
  {
    this.subAll = this.utils.getAllUsers().subscribe(data => 
      {
        this.usersList = data
      })
  }

  async filter()
  {
    if(this.search!=null)
      {
        this.usersList = await this.utils.search(this.search)
      }
      else
      {
        this.getAll()
      }

  }


  visibleTasksPosts(user: User)
  {
    this.TasksPosts = true
    this.focusUser = user
  }

  ngOnDestroy(): void {
    this.subAll.unsubscribe()
  }

}
