import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Post } from '../post';
import { Task } from '../task';
import { UserService } from '../user.service';

@Component({
  selector: 'app-posts-tasks-cols',
  templateUrl: './posts-tasks-cols.component.html',
  styleUrls: ['./posts-tasks-cols.component.css']
})
export class PostsTasksColsComponent implements OnInit {

  @Input()
  focusUser: any;
  newTask: Task = {title: null, completed: false}
  newPost: Post = {title: null, body: null}
  newTaskToggle: boolean = false
  newPostToggle: boolean = false
  subPut: Subscription;
  subGet: Subscription;
  
  constructor(private utils: UserService, private router: Router) { }

  addTask()
  {
    if(this.newTask.title!=null)
    {
      this.focusUser.Tasks.push(this.newTask)
      this.subPut = this.utils.putUser(this.focusUser._id, this.focusUser).subscribe(data => {
        console.log(data)
        this.subGet = this.utils.getAUser(this.focusUser._id).subscribe(refresh => this.focusUser=refresh)
      })
      
    }
  }

  addPost()
  {
    if(this.newPost.title!=null && this.newPost.body!=null)
    {
      this.focusUser.Posts.push(this.newPost)
      this.subPut = this.utils.putUser(this.focusUser._id, this.focusUser).subscribe(data => {
        console.log(data)
        this.subGet = this.utils.getAUser(this.focusUser._id).subscribe(refresh => this.focusUser=refresh)
      })
      
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy()
  {
    if(this.subPut!=null)
    {this.subPut.unsubscribe()}

    if(this.subGet!=null)
    {this.subGet.unsubscribe()}
  }

}
