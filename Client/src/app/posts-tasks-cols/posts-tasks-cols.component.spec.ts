import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsTasksColsComponent } from './posts-tasks-cols.component';

describe('PostsTasksColsComponent', () => {
  let component: PostsTasksColsComponent;
  let fixture: ComponentFixture<PostsTasksColsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostsTasksColsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsTasksColsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
