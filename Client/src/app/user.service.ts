import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string = 'http://localhost:2000/users/'

  constructor(private http: HttpClient) { }

  getAllUsers()
  {
    return this.http.get<User[]>(this.url)
  }

  getAUser(id: string)
  {
    return this.http.get<User>(`${this.url}${id}`)
  }

  postUser(user: User)
  {
    return this.http.post(this.url, user)
  }

  putUser(id: string, user: User)
  {
    return this.http.put(`${this.url}${id}`, user)
  }

  deleteUser(id: string)
  {
    return this.http.delete(`${this.url}${id}`)
  }

  async search(search: string)
  {
    let all = await this.http.get<User[]>(this.url).toPromise()
    
    let result = []
    all.forEach(user => 
      {
        if(user.Name.toLowerCase().includes(search.toLowerCase()) || user.Email.toLowerCase().includes(search.toLowerCase()))
        {
          result.push(user)
        }
      })
    return result;
  }

}


